﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameScript : MonoBehaviour {

    public void Start()
    {
        GameObject p = GameObject.Find("Player");

        p.transform.position=new Vector3(0, 0, 0);
        //p.transform.Rotate(0, 180, 0);
        p.GetComponent<PlayerController>().LevelNumber = 1;
        p.GetComponent<PlayerController>().Goal = 1;
        p.GetComponent<PlayerController>().KilledEnemies = 0;

        p.GetComponent<PlayerController>().Level.text = "Level: 1";
        p.GetComponent<PlayerController>().GoalText.text = "Goal: 1";
        p.GetComponent<PlayerController>().Killed.text = "Killed Enemies: 0";

        GameObject sp = GameObject.Find("Spawner");
        sp.GetComponent<SpawningScript>().InitializeSpawn(1);

        //deleting all enemies
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemyTag");
        foreach (GameObject current in enemies)
        {
            GameObject.Destroy(current);
        }

    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "bulletTag")
            Start();
    }
}
