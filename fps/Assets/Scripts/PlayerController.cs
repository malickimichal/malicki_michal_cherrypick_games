﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    bool test = true;
    public float movementSpeed = 5.0f;
    public float mouseSesitivity = 2.0f;
    public float PlayerHealth = 2f;
    public Text Health;
    public Text Level;
    public Text GoalText;
    public Text Killed;
    public int LevelNumber = 1;
    public int KilledEnemies = 0;
    public int Goal = 1;
    // Use this for initialization
    void Start () {
        Screen.lockCursor = true;
        Health.text = "Health: 10";//PlayerHealth.ToString();
        Level.text = "Level: 1";
	}
	
	// Update is called once per frame
	void Update () {
        CharacterController cs = GetComponent<CharacterController>();
        if(test)
        {
            Die();
            test = false;
        }
        if (Input.GetKey("escape"))
            Die();

            //moevement
            float forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
        float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;
        Vector3 speed = new Vector3(sideSpeed, 0, forwardSpeed);
        speed = transform.rotation * speed;
        cs.SimpleMove(speed);

        //rotating 
        float rotateX = Input.GetAxis("Mouse X")*mouseSesitivity;
        float rotateY = Input.GetAxis("Mouse Y")*mouseSesitivity;

        float currentRotation = Camera.main.transform.rotation.eulerAngles.x;
        float desiredRotation = currentRotation - rotateY;

        Camera.main.transform.localRotation = Quaternion.Euler(desiredRotation, 0, 0); //przez zastosowanie eulera kąt nie przekroczy 90 stopni (chyba)
        transform.Rotate(0,rotateX,0);

 


        

    }
    //getting hit by enemy
    public void GetDamage()
    {
        PlayerHealth--;
        Health.text = "Health: " + PlayerHealth.ToString();

        if (PlayerHealth <= 0)
            Die();
    }
    //dying
    public void Die()
    {
        //going to menu
        transform.position = new Vector3(-0.1861165f, 3.92601f, -46.70956f);
        
        //health to 10 again
        PlayerHealth = 10f;
        Health.text = "Health: "+ PlayerHealth.ToString();
    }
    public void AddDeathCount()
    {
        KilledEnemies++;
        Killed.text = "Killed Enemies: " + KilledEnemies.ToString();
        if (KilledEnemies == Goal)
            LevelUp();
    }
    public void LevelUp()
    {

        LevelNumber++;
        Goal += LevelNumber;
        Level.text = "Level: " + LevelNumber.ToString();
        GoalText.text = "Goal: " + Goal.ToString();

        
        GameObject p = GameObject.Find("Spawner");
       p.GetComponent<SpawningScript>().InitializeSpawn(LevelNumber);
    }

}
