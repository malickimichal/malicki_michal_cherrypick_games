﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class EnemyMovement : MonoBehaviour {
    public float enemySpeedDividor = 15.0f;
    public float enemyHealth = 3f;
    

    // public Text Health;
    // Use this for initialization
    void Start () {

       // Health.text = enemyHealth.ToString() ;
}
	
	// Update is called once per frame
	void Update () {
        Camera cam = Camera.main;
    Vector3 movementVector = -(transform.position - cam.transform.position)/enemySpeedDividor;
        GetComponent<Rigidbody>().AddForce(movementVector, ForceMode.Impulse);
        


    }
    void OnCollisionEnter(Collision col)
    {
     Camera cam = Camera.main;
        if (col.gameObject.tag == "bulletTag")
        {
            enemyHealth--;
            // Health.text = enemyHealth.ToString();
            if (enemyHealth <= -1)
            {
                EnemyDies();
            }
        }
        if (col.gameObject.tag == "playerTag")
        {
            GetComponent<Rigidbody>().AddForce((transform.position - cam.transform.position)*5f, ForceMode.Impulse);
            PlayerController pc = col.gameObject.GetComponent<PlayerController>();
            pc.GetDamage();


        }


        if (enemyHealth<=0)
            GetComponent<Rigidbody>().AddForce(new Vector3(0,8,0), ForceMode.Impulse);

    }
    void EnemyDies()
    {
        Destroy(gameObject);
        GameObject p  = GameObject.Find("Player");
        p.GetComponent<PlayerController>().AddDeathCount();

    }
}
