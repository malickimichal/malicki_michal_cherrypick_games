﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {
    public float bulletSpeed = 50.0f;
    public float interval = 0.5f;
    public float time = 0;
    public float rtime = 0f;
    public GameObject bulletPrefab;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       // rtime += Time.deltaTime;
        if (time < interval)
        {
            time += Time.deltaTime;
        }
        Camera cam = Camera.main;
        if (Input.GetButton("Fire1") && time>=interval)
        {
            time = 0;
           GameObject movingBullet =  Instantiate(bulletPrefab, cam.transform.position + cam.transform.forward, cam.transform.rotation);
            //movingBullet.rigidbody.AddForce(cam.transform.forward * bulletSpeed, ForceMode.Impulse);
            movingBullet.GetComponent<Rigidbody>().AddForce(cam.transform.forward * bulletSpeed, ForceMode.Impulse);
            Debug.Log("bang" + rtime);
        }


	}
}
